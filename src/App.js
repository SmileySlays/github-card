import React from "react";
import "./App.css";
import { Card } from "semantic-ui-react";

class Toggle extends React.Component {
  constructor(props) {
    super(props);
    this.state = { active: false, user: {} };
    //console.log(this.state.user);
    // This binding is necessary to make `this` work in the callback
    this.handleClick = this.handleClick.bind(this);
  }

  handleClick() {
    fetch("https://api.github.com/users/jackeyGao").then(res =>
      res.json().then(data => this.setState({ user: data }))
    );

    this.setState(state => ({
      active: !state.active
    }));

    //console.log(this.state);
  }
  //bio,
  render() {
    return (
      <div>
        <button onClick={this.handleClick}> Toggle </button>

        {this.state.active ? (
          <Card
            image={this.state.user.avatar_url}
            header={this.state.user.name}
            meta={this.state.user.location}
            description={this.state.user.bio}
          />
        ) : (
          console.log("OFF")
        )}
      </div>
    );
  }
}

export default Toggle;
